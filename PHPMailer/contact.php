<html>
	<head>
		<link rel="stylesheet" href="./css/reset.css"/>
		<link rel="stylesheet" href="./css/screen.css"/>
	</head>
	<body>
		<div id="wrapper">
			<!--<?php include "./includes/header.php" ?>-->
			<!--<?php include "./includes/nav.php" ?>-->
			
			<div id="content" class="contAside">
				<h1>Kontakt</h1><br/>
				<h2>Wir freuen uns auf Ihre Anfrage!</h2><br/>
				
				<?php
				if(isset($_POST['submitBtn']))
				{
					require 'class.phpmailer.php';
					//include 'accountInfo.php';
					
					$mail = new PHPMailer;

					$mail->isSMTP();                                      // Set mailer to use SMTP
					$mail->Host = 'smtp.gmail.com'; 					  // Specify main and backup server
					$mail->SMTPAuth = true;                               // Enable SMTP authentication
					$mail->Username = 'htlzwettl.itp.project@gmail.com';                            // SMTP username
					$mail->Password = 'htlzwettl';                           // SMTP password
					$mail->SMTPSecure = 'ssl';
					$mail->Port=465;                            // Enable encryption, 'ssl' also accepted

					$mail->From = $_POST['email'];
					$mail->FromName = 'DiamondDesign FROM'.$_POST['nn'].' '.$_POST['vn'];
					$mail->addAddress('htlzwettl.itp.project@gmail.com', 'Information');  // Add a recipient
					$mail->addReplyTo($_POST['email'], 'Antwort auf ihre Kontaktanfrage');
					$mail->addCC($_POST['email']);
					// $mail->addBCC('bcc@example.com');

					$mail->WordWrap = 70;                                 // Set word wrap to 50 characters
					$mail->isHTML(true);                                  // Set email format to HTML

					$mail->Subject = $_POST['reason'];
					$mail->Body    = $_POST['text'];
					//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
					$tempSendInfo = $mail->send();
					if(!$tempSendInfo) {
					   echo 'Message could not be sent.</br>';
					   echo 'Mailer Error: ' . $mail->ErrorInfo;
					   exit;
					}

					echo 'Message has been sent';
				}	
				?>
				
				<form  method="POST">
				
					<table>
			<th> Kontaktformular </th>
			<tr>
				<td class="text-rechts"> Anrede:</td>
				
				<td>
					<select size="1" name="anrede">
						<option>Herr</option>
						<option>Frau</option>
					</select>
				</td>
			</tr>
		
			<tr>
				<td class="text-rechts">
					Vorname/Nachname:
				</td>
				<td>
					<input type="text" name="Vorname" value="Max"  required/>
					<input type="text" name="Nachname" value="Mustermann"  required/>
				</td>
			</tr>
			
			<tr>
				<td class="text-rechts">
					Strasse:
				</td>
				<td>
					<input type="text" name="Strasse" />
				</td>
			</tr>
			
			<tr>
				<td class="text-rechts">
					PLZ/Ort:
				</td>
				
				<td>
					<input type="number" name="PLZ" />
					<input type="text" name="Ort" />
				</td>
			</tr>
			
			<tr>
				<td class="text-rechts">
					Land:
				</td>
				
				<td>
					<select size="1" name="Land">
						<option>&Ouml;sterreich</option>
						<option>Schweiz</option>
						<option>Deutschland</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td class="text-rechts">
					Telefonnummer:
				</td>
				
				<td>
					<input type="number" name="Telefonnummer" />
				</td>
			</tr>
			
			<tr>
				<td class="text-rechts">
					E-Mail:
				</td>
				<td>
					<input type="email" name="email">
				</td>
			</tr>
			
			<tr>
				<td class="text-rechts" valign="top">Grund ihrer Nachricht:</td>
				<td>
					<input type="radio" name="zusatz1" value="Ich habe eine Frage">Ich habe eine Frage</input> <br>
					<input type="radio" name="zusatz2" value="Ich habe einen Vorschlag f&uuml;r Ihre Webseite">Ich habe einen Vorschlag f&uuml;r Ihre Webseite</input><br>
					<input type="radio" name="zusatz3" value="Ich habe eine Kritik anzubringen">Ich habe eine Kritik anzubringen</input> <br>
				</td>
			</tr>
			
			<tr>
				<td class="text-rechts" valign="top">
					Text:
				</td>
			
				<td>
				<textarea name="Textbox" style="max-width:250px; min-width: 250px; max-height:50px; min-height: 70px; "rows="4" cols="50" ></textarea>
				</td>
			</tr>
			<br>
			<tr>
				<td class="text-rechts" valign="top">Gew&uuml;nscher Kontakt:</td>
				<td>
					<input type="radio" name="kontakt" value="E-Mail">E-mail</input> <br>
					<input type="radio" name="kontakt" value="Telefon">Telefon</input> <br>
					<input type="radio" name="kontakt" value="nicht notwendig">nicht notwendig</input> <br> 
			</tr>
			
			
			<tr>
			
				<td class="text-rechts" valign="top">
					Gew&uuml;nschter Newsletter:
				</td>
				
				<td>
					<input type="checkbox" 	name="Newsletter[]" value="Software-Engineering">Software-Engineering</input> <br>
					<input type="checkbox" 	name="Newsletter[]" value="Software-Qualit&auml;tssicherung">Software-Qualit&auml;tssicherung</input> <br>
					<input type="checkbox"	name="Newsletter[]" value="Softwareentwicklungs-Vorgehensmodelle">Softwareentwicklungs-Vorgehensmodelle</input> <br>
					<input type="checkbox"	name="Newsletter[]" value="Test Driven Develoment">Test Driven Develoment</input>
				</td>
				
			</tr>
			
			<tr>
				<td>
					<input type="submit" name="submit" value="Absenden">
				</td>
			</tr>
			
		</table>
	</body>
</html>