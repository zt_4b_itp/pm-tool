-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 13. Nov 2014 um 09:36
-- Server Version: 5.6.16
-- PHP-Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `pmtool`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Package_ID` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `content` longblob NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_document_Package1_idx` (`Package_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `environment`
--

CREATE TABLE IF NOT EXISTS `environment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Project_ID` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `rating` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  PRIMARY KEY (`ID`,`Project_ID`),
  KEY `fk_Environment_Project1_idx` (`Project_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `package`
--

CREATE TABLE IF NOT EXISTS `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `User_ID` int(11) DEFAULT NULL,
  `Project_ID` int(11) NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Package_Team1_idx` (`User_ID`,`Project_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `projektleitertest`
--

CREATE TABLE IF NOT EXISTS `projektleitertest` (
  `ID` int(11) NOT NULL,
  `Projektleiter` varchar(200) NOT NULL,
  `EMail` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `projektleitertest`
--

INSERT INTO `projektleitertest` (`ID`, `Projektleiter`, `EMail`) VALUES
(1, 'Honeder Julia', 'hochstoeger.gernot@gmail.com'),
(2, 'Strabler Thomas', 'thomas5.strabler@gmail.com'),
(3, 'Leutgeb Andreas', 'andeleutgeb@gmail.com');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `risk`
--

CREATE TABLE IF NOT EXISTS `risk` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Environment_ID` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `probability` int(11) NOT NULL,
  `impact` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Risk_Environment1_idx` (`Environment_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `userinproject`
--

CREATE TABLE IF NOT EXISTS `userinproject` (
  `User_ID` int(11) NOT NULL,
  `Project_ID` int(11) NOT NULL,
  `rights` int(11) NOT NULL,
  `supervisor_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`User_ID`,`Project_ID`),
  KEY `fk_User_has_Project_Project1_idx` (`Project_ID`),
  KEY `fk_User_has_Project_User_idx` (`User_ID`),
  KEY `fk_UserInProject_UserInProject1_idx` (`supervisor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `fk_document_Package1` FOREIGN KEY (`Package_ID`) REFERENCES `package` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `environment`
--
ALTER TABLE `environment`
  ADD CONSTRAINT `fk_Environment_Project1` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `package`
--
ALTER TABLE `package`
  ADD CONSTRAINT `fk_Package_Team1` FOREIGN KEY (`User_ID`, `Project_ID`) REFERENCES `userinproject` (`User_ID`, `Project_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `risk`
--
ALTER TABLE `risk`
  ADD CONSTRAINT `fk_Risk_Environment1` FOREIGN KEY (`Environment_ID`) REFERENCES `environment` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `userinproject`
--
ALTER TABLE `userinproject`
  ADD CONSTRAINT `fk_UserInProject_UserInProject1` FOREIGN KEY (`supervisor_ID`) REFERENCES `userinproject` (`User_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_User_has_Project_Project1` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_User_has_Project_User` FOREIGN KEY (`User_ID`) REFERENCES `user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
