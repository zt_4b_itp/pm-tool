-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 13. Nov 2014 um 08:53
-- Server Version: 5.5.27
-- PHP-Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `pm-tools`
--
use pmtool;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `apcategory`
--

CREATE TABLE IF NOT EXISTS `apcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `short` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `short_UNIQUE` (`short`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `apcategory`
--

INSERT INTO `apcategory` (`ID`, `name`, `short`) VALUES
(4, 'Grobplanung', 'G_'),
(5, 'Feinplanung', 'F_'),
(6, 'Nachprojektphase', 'N_');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Package_ID` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `content` longblob NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_document_Package1_idx` (`Package_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `environment`
--

CREATE TABLE IF NOT EXISTS `environment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Project_ID` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `rating` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Environment_Project1_idx` (`Project_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `package`
--

CREATE TABLE IF NOT EXISTS `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `APCategory_ID` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `start_planned` datetime NOT NULL,
  `end_planned` datetime NOT NULL,
  `puffer` datetime DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `User_ID` int(11) DEFAULT NULL,
  `Project_ID` int(11) NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Package_Team1_idx` (`User_ID`,`Project_ID`),
  KEY `fk_Package_APCategory1_idx` (`APCategory_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Daten für Tabelle `package`
--

INSERT INTO `package` (`ID`, `APCategory_ID`, `name`, `description`, `start_planned`, `end_planned`, `puffer`, `start`, `end`, `User_ID`, `Project_ID`, `done`) VALUES
(9, 4, '01', 'Teamliste erstellen', '2014-11-13 04:00:00', '2014-11-14 07:00:00', NULL, NULL, NULL, 5, 3, 0),
(10, 5, '02', 'Pflichtenheft erstellen', '2014-11-16 05:00:00', '2014-11-18 08:00:00', NULL, NULL, NULL, 4, 3, 0),
(11, 5, '01', 'PSP erstellen', '2014-11-20 06:00:00', '2014-11-21 15:00:00', NULL, NULL, NULL, 4, 3, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `project`
--

INSERT INTO `project` (`ID`, `name`) VALUES
(3, 'TestProject1'),
(4, 'TestProject2');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `risk`
--

CREATE TABLE IF NOT EXISTS `risk` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Environment_ID` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `probability` int(11) NOT NULL,
  `impact` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Risk_Environment1_idx` (`Environment_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`ID`, `name`, `password`, `email`) VALUES
(4, 'TestUser1', '1234', 'test@testfirma.com'),
(5, 'TestUser2', '1234', 'testuser2@testemail.com');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `userinproject`
--

CREATE TABLE IF NOT EXISTS `userinproject` (
  `User_ID` int(11) NOT NULL,
  `Project_ID` int(11) NOT NULL,
  `rights` int(11) NOT NULL,
  `supervisor_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`User_ID`,`Project_ID`),
  KEY `fk_User_has_Project_Project1_idx` (`Project_ID`),
  KEY `fk_User_has_Project_User_idx` (`User_ID`),
  KEY `fk_UserInProject_UserInProject1_idx` (`supervisor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `userinproject`
--

INSERT INTO `userinproject` (`User_ID`, `Project_ID`, `rights`, `supervisor_ID`) VALUES
(4, 3, 1, NULL),
(4, 4, 2, 5),
(5, 3, 2, 4),
(5, 4, 1, NULL);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `fk_document_Package1` FOREIGN KEY (`Package_ID`) REFERENCES `package` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `environment`
--
ALTER TABLE `environment`
  ADD CONSTRAINT `fk_Environment_Project1` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `package`
--
ALTER TABLE `package`
  ADD CONSTRAINT `fk_Package_Team1` FOREIGN KEY (`User_ID`, `Project_ID`) REFERENCES `userinproject` (`User_ID`, `Project_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Package_APCategory1` FOREIGN KEY (`APCategory_ID`) REFERENCES `apcategory` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `risk`
--
ALTER TABLE `risk`
  ADD CONSTRAINT `fk_Risk_Environment1` FOREIGN KEY (`Environment_ID`) REFERENCES `environment` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `userinproject`
--
ALTER TABLE `userinproject`
  ADD CONSTRAINT `fk_User_has_Project_User` FOREIGN KEY (`User_ID`) REFERENCES `user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_User_has_Project_Project1` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_UserInProject_UserInProject1` FOREIGN KEY (`supervisor_ID`) REFERENCES `userinproject` (`User_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
