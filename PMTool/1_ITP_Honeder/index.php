<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    
  </head>
  <body>
    <nav class="navbar-fixed-top navbar-default" role="navigation">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://www.htlkrems.ac.at">HTL ZWETTL</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Datei <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bearbeiten <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ansicht <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Einfügen <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Werkzeuge <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Projekt <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hilfe <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Info <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Language <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">German</a></li>
            <li><a href="#">English</a></li>
          </ul>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="http://www.htlkrems.ac.at"> <img src="htl_krems_logo.png" width="150px" height="30px"></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
    <div class="btn-toolbar" role="toolbar">
      <div class="btn-group">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-file"></span> <span class="sr-only">Neu</span></button>
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-folder-open"></span> <span class="sr-only">Öffnen</span></button>
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-floppy-disk"></span> <span class="sr-only">Speichern</span></button>
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> <span class="sr-only">Drucken</span></button>
      </div>
    </div>
  </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <div class="btn-group">
        <div class="btn-toolbar" role="toolbar">
        
          <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-off"></span>
          </button>
        </div>
      </div>
      </ul>
    </div><!-- /.navbar-collapse -->
</nav>

<div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="#" align ="center">Kategorie1</a></li>
            <li><a href="#" align ="center">Kategorie2</a></li>
            <li><a href="#" align ="center">Kategorie3</a></li>
            <li><a href="#" align ="center">Kategorie4</a></li>

          </ul>
          </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

          <h1 class="sub-header">Gefundene Artikel</h1>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Artikel</th>
                  <th></th>
                  <th></th>
                  <th>Preis</th>
                  <th>Warenkorb</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Hemd</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default">
                  <span class="glyphicon glyphicon-shopping-cart"></span> 
                  <span class="sr-only">Center Align</span></button>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Jacke</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default">
                  <span class="glyphicon glyphicon-shopping-cart"></span> 
                  <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Hose</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>BH</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Slip</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>Schlüpfer</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>Jean</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>8</td>
                  <td>Jogging Hose</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>9</td>
                  <td>T-Shirt</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>10</td>
                  <td>Auto</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>11</td>
                  <td>LKW</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>12</td>
                  <td>Buch</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>13</td>
                  <td>Handy</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
                <tr>
                  <td>14</td>
                  <td>Gitarre</td>
                  <td></td>
                  <td>12€</td>
                  <td><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="sr-only">Center Align</span></button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>