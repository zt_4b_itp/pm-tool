use pmtool;
INSERT INTO `environment`(`Project_ID`, `description`,`rating`,`area`) VALUES
	(1,'env1',1,1),(2,'env2',0,2),(3,'env3',-1,2),(2,'env4',0,1);

inSERT INTO `user`(`name`, `password`, `email`) VALUES
('Honeder Julia','pwdHon', 'hochstoeger.gernot@gmail.com'),
('Strabler Thomas','pwdStr', 'thomas5.strabler@gmail.com'),
('Leutgeb Andreas','pwdLeu', 'andeleutgeb@gmail.com');

INSERT INTO `project`(`name`) VALUES('PMTool'),('Webshop'),('WIR-Firma');

INSERT INTO `document`(`Package_ID`,`name`,`type`,`size`,`content'` VALUES
	(1,'doc1','typ1',3,1),(1,'doc2','typ2',4,0);

INSERT INTO `risk`(`Environment_ID`,`description`,`probability`,`impact`) VALUES
	(1,'death of a programmer',5,100),
	(2,'Apache stops working',20,50),
	(5,'Teacher never uses this project other than in the first lesson',10,100);

