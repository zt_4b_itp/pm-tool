<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="loginStyle.css"/>
	<script src="jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
	<script>
		function createSession() {
			var name = document.getElementsByName('user')[0].value;
			var pwd = document.getElementsByName('pwd')[0].value;
			document.getElementsByName('errorBox')[0].style.display = 'none';
			$.ajax({
				data: "user="+name+"&pwd="+pwd,
				type: "GET",
				url: "createSession.php",
				success: function(response,par2,par3) {
					self.location.href = "index.php";
				},
				error: function(par1,par2,err) {
					document.getElementsByName('errorBox')[0].style.display = 'block';
				}
			});
		}
	</script>
</head>
<body>
	<div class="container">
	<div id="errorBoxId" class="alert alert-danger" name="errorBox">
		The username or password is incorrect!
	</div>
	<form class="form-signin" id="loginForm" role="form">
		<h2 class="form-signin-heading">Login-Data</h2>
		<input class="form-control" type="text" id="nameID" name="user" autofocus="" placeholder="User name" />
		<input class="form-control" type="password" id="pwdID" name="pwd" placeholder="Password" />
		<button class="btn btn-lg btn-primary btn-block" type="submit" onclick="createSession(); return false;">Sign in</button>
	</form>
	</div>
</body>
</html>