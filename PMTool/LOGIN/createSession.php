<?php
session_destroy();
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')
	die('Nur Ajax-Requests m�glich!');
$user = $_GET["user"];
$pwd = $_GET["pwd"];
require 'db.php';
$query = $db->prepare('select * from user');
$query->bindParam(":name", $user);
$query->bindParam(":pwd", $pwd);
$query->execute();
$id = $query->fetch()[0];
if ($id == -1) {
	header("HTTP/1.1 500 Internal Server Error");
}
else {
	session_start();
	$_SESSION['USER_ID'] = $id;
	$_SESSION['USER_NAME'] = $user;
}
?>