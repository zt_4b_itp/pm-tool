<html>

<head>
	<meta charset="UTF-8">
	<title>Home</title>
  	<link href="bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<body>
<div>
	<div class="container">
		<div class="page-header">
			<h1>Home</h1>
		</div>
	<?php
		session_start();
		if (!isset($_SESSION['USER_NAME'])) {
			echo'<a href = "login.php"><button type="button" class="btn btn-default">Login</button></a>';
		}
		else{
			echo 'logged in as <b>'.$_SESSION['USER_NAME'].'</b><br>';
			echo '<div class="btn-group" role="group">';
			
			if ($_SESSION['USER_NAME'] == "admin") {
				echo '<a href = "register.php"><button type="button" class="btn btn-default">Create User</button></a>';
				echo '<a href = "show_users.php"><button type="button" class="btn btn-default">Show all users</button></a>';
			}
			else if (isset($_SESSION['USER_NAME'])) {
				echo '<a href = "uebersicht.php"><button type="button" class="btn btn-default">Overview</button></a>';
				echo '<a href = "delete_edit_user.php"><button type="button" class="btn btn-default">Edit</button></a>';				
			}
			echo '<a href="logout.php"><button type="button" class="btn btn-default">Logout</button></a>';
			echo '</div>';
			}	
	?>
	</div>
</div>
</body>

</html>